<?php

namespace Ottivo\Service\DataProvider;

use Ottivo\Entity\Employee;

class XMLEmployeeDataProvider implements EmployeeDataProviderInterface
{
    /**
     * @var \SimpleXMLElement
     */
    private $xmlData;

    /**
     * @param string $filePath
     *
     * @throws \UnexpectedValueException
     */
    public function __construct(string $filePath)
    {
        if (!file_exists($filePath) || !is_readable($filePath)) {
            throw new \UnexpectedValueException(sprintf(
                'File %s is inaccessible.',
                $filePath
            ));
        }

        $this->xmlData = simplexml_load_file($filePath);
    }

    /**
     * @return array|Employee[]
     *
     * @throws \Exception
     */
    public function getEmployees(): array
    {
        $employees = [];

        foreach ($this->xmlData->employee as $employeeData) {
            $employee = new Employee();

            $employee->setName($employeeData->name)
                ->setDateOfBirth(new \DateTime($employeeData->birthday))
                ->setContractStartDate(new \DateTime($employeeData->contract_start))
                ->setVacationDays(intval($employeeData->special_contract_vacation_days) ?: 0);

            $employees[] = $employee;
        }

        return $employees;
    }
}
