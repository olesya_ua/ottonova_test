<?php

namespace Ottivo\Service\DataProvider;

use Ottivo\Entity\Employee;

interface EmployeeDataProviderInterface
{
    /**
     * @return array|Employee[]
     */
    public function getEmployees(): array;
}
