<?php

namespace Ottivo\Entity;

class Employee
{
    const MIN_VACATION_DAYS = 26;

    const AGE_EMPLOYEE_ADDITIONAL_VACATION = 30;

    const YEARS_WORKED_ADDITIONAL_VACATION = 5;

    const VACATION_DAYS_PER_MONTH_COEFFICIENT = 1/12;

    /** @var string */
    private $name;

    /** @var \DateTime */
    private $dateOfBirth;

    /** @var \DateTime */
    private $contractStartDate;

    /** @var integer */
    private $vacationDays;

    public function setName(string $name): Employee
    {
        $this->name = $name;

        return $this;
    }

    public function setDateOfBirth(\DateTime $dateOfBirth): Employee
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function setContractStartDate(\DateTime $contractStartDate): Employee
    {
        $this->contractStartDate = $contractStartDate;

        return $this;
    }

    public function setVacationDays(int $vacationDays): Employee
    {
        $this->vacationDays = $vacationDays;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param int $year
     * @return int
     * @throws \DomainException when employee has been hire later than $year
     * @throws \Exception
     */
    public function getNumberOfVacationDays(int $year): int
    {
        if ($year < $this->contractStartDate->format('Y')) {
            throw new \DomainException(sprintf(
                '%s haven\'t started a contract at Ottivo yet.',
                $this->name
            ));
        }

        $vacationDays = max($this->vacationDays, self::MIN_VACATION_DAYS);

        if ($this->contractStartDate->format('Y') == $year) {
            $vacationDays = $this->calculateVacationPerMonth($vacationDays);
        }

        if ($vacationDays > 0) {
            $vacationDays += $this->calculateAdditionalVacationForAge($year);
        }

        return floor($vacationDays);
    }

    /**
     * @param int $yearlyVacationDays
     *
     * @return int
     *
     * @throws \Exception
     */
    private function calculateVacationPerMonth(int $yearlyVacationDays): int
    {
        $fullMonthsNumber = $this->contractStartDate->diff(new \DateTime(
            sprintf('31.12.%s', $this->contractStartDate->format('Y'))
        ))->m;

        if ($this->contractStartDate->format('d') == 1) {
            $fullMonthsNumber ++;
        }

        return $fullMonthsNumber * $yearlyVacationDays * self::VACATION_DAYS_PER_MONTH_COEFFICIENT;
    }

    /**
     * @param int $year
     *
     * @return int
     *
     * @throws \Exception
     */
    private function calculateAdditionalVacationForAge(int $year): int
    {
        $additionalVacationDays = 0;

        $employeeAge = $this->dateOfBirth->diff(new \DateTime(sprintf('31.12.%s', $year)))->y;
        if ($employeeAge >= self::AGE_EMPLOYEE_ADDITIONAL_VACATION) {
            $employeeAgeDiff = $employeeAge - self::AGE_EMPLOYEE_ADDITIONAL_VACATION;
            $additionalVacationDays = $employeeAgeDiff / self::YEARS_WORKED_ADDITIONAL_VACATION;
        }

        return $additionalVacationDays;
    }
}
