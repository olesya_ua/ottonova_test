<?php

namespace Ottivo\Command;

use Ottivo\Service\DataProvider\EmployeeDataProviderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmployeesVacationCommand extends Command
{
    protected static $defaultName = 'ottivo:employees-vacation';

    /** @var EmployeeDataProviderInterface*/
    private $dataProvider;

    public function __construct(EmployeeDataProviderInterface $dataProvider)
    {
        parent::__construct(self::$defaultName);

        $this->dataProvider = $dataProvider;
    }

    protected function configure()
    {
        $this->setDescription('Shows the number of vacation days of employees.')
            ->setHelp('This command allows you to see the number of vacation days of employees.')
            ->addArgument(
                'year',
                InputArgument::OPTIONAL,
                'A year that you interested to see vacation days',
                date('Y')
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $year = $input->getArgument('year');

        $output->writeln(sprintf('Number of vacation days in %s', $year));
        $output->writeln('-------------------------------');

        foreach ($this->dataProvider->getEmployees() as $employee) {

            try {
                $output->writeln(sprintf(
                    '%s - %s',
                    $employee->getName(),
                    $employee->getNumberOfVacationDays($year)
                ));
            } catch (\DomainException $exception) {
                $output->writeln($exception->getMessage());
            }
        }
    }
}
