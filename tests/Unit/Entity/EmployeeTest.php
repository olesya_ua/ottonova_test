<?php

namespace Ottivo\Tests\Unit\Entity;

use Ottivo\Entity\Employee;
use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase
{
    /**
     * @param integer $year
     * @param integer $vacationDays
     * @param array $employeeData
     *
     * @dataProvider producePositiveDataProvider
     * @throws \Exception
     */
    public function testProducePositive(int $year, int $vacationDays, array $employeeData)
    {
        $employee = new Employee();

        $employee->setName($employeeData['name'])
            ->setDateOfBirth(new \DateTime($employeeData['birthday']))
            ->setContractStartDate(new \DateTime($employeeData['contract_start']))
            ->setVacationDays($employeeData['special_contract_vacation_days']);

        $this->assertEquals($vacationDays, $employee->getNumberOfVacationDays($year));
    }

    public function producePositiveDataProvider(): array
    {
        return [
            [
                2010,
                31,
                [
                    'name' => 'James Hetfield',
                    'birthday' => '03.08.1963',
                    'contract_start' => '01.01.2010',
                    'special_contract_vacation_days' => 28,
                ]
            ],
            [
                2010,
                19,
                [
                    'name' => 'James Hetfield',
                    'birthday' => '03.08.1963',
                    'contract_start' => '01.06.2010',
                    'special_contract_vacation_days' => 28,
                ]
            ],
            [
                2015,
                17,
                [
                    'name' => 'Eicca Toppinen',
                    'birthday' => '05.08.1975',
                    'contract_start' => '15.05.2015',
                    'special_contract_vacation_days' => 0,
                ]
            ],
            [
                2015,
                0,
                [
                    'name' => 'Eicca Toppinen',
                    'birthday' => '05.08.1975',
                    'contract_start' => '15.12.2015',
                    'special_contract_vacation_days' => 0,
                ]
            ],
        ];
    }

    /**
     * @param integer $year
     * @param array $employeeData
     *
     * @dataProvider produceNegativeDataProvider
     * @throws \Exception
     */
    public function testProduceNegative(int $year, array $employeeData)
    {
        $this->expectException(\DomainException::class);

        $employee = new Employee();

        $employee->setName($employeeData['name'])
            ->setDateOfBirth(new \DateTime($employeeData['birthday']))
            ->setContractStartDate(new \DateTime($employeeData['contract_start']))
            ->setVacationDays($employeeData['special_contract_vacation_days']);

       $employee->getNumberOfVacationDays($year);
    }

    public function produceNegativeDataProvider(): array
    {
        return [
            [
                2009,
                [
                    'name' => 'James Hetfield',
                    'birthday' => '03.08.1963',
                    'contract_start' => '01.01.2010',
                    'special_contract_vacation_days' => 28,
                ]
            ],
        ];
    }
}
