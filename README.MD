# Test task

## Installation
```
composer install
```

## Command
The example of the command, where 2015 is the year of interest:
```
php bin/console ottivo:employees-vacation 2015
```
If you omit the year argument, then it will be set to current year:
```
php bin/console ottivo:employees-vacation
```
## Tests
```
php bin/phpunit
```

## Ambiguities
This requirement "Employees >= 30 years get one additional vacation day every 5 years" can be understood in two ways: 

1. Employees over 30 years get one additional vacation day for each 5 years of the age over 30.
2. Employees over 30 years get one additional vacation day for each 5 years of employment in this company.

And I have chosen the first one :)  